const path = require("path");
const problem6 = require("../callback6");

problem6(
  "Thanos",
  path.join(__dirname, "../data/boards.json"),
  path.join(__dirname, "../data/lists.json"),
  path.join(__dirname, "../data/cards.json")
);
