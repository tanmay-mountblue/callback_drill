const path = require("path");
const problem4 = require("../callback4");

problem4(
  "Thanos",
  "Mind",
  path.join(__dirname, "../data/boards.json"),
  path.join(__dirname, "../data/lists.json"),
  path.join(__dirname, "../data/cards.json")
);
