const path = require("path");
const getBoardInformaton = require("../callback1");
const callback = (err, data) => {
  if (err) {
    console.error("Failure");
    console.error(err.message);
  } else {
    console.log("Success");
    console.log(data);
  }
};

getBoardInformaton(
  path.join(__dirname, "../data/boards.json"),
  "mcu453ed",
  callback
);

getBoardInformaton(
  path.join(__dirname, "../data/board.json"),
  "mcu453ed",
  callback
);

getBoardInformaton(
  path.join(__dirname, "../data/boards.json"),
  "mcu453e",
  callback
);

getBoardInformaton(
  path.join(__dirname, "../data/boards.json"),
  [],
  callback
);

getBoardInformaton(
  path.join(__dirname, "../data/boards.json"),
  "mcu453ed"
);