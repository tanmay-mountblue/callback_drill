const path = require("path");
const getCardsForListID = require("../callback2");
const callback = (err, data) => {
  if (err) {
    console.error("Failure");
    console.error(err.message);
  } else {
    console.log("Success");
    console.log(data);
  }
};

getCardsForListID(
  path.join(__dirname, "../data/cards.json"),
  "qwsa221",
  callback
);

getCardsForListID(
  path.join(__dirname, "../data/card.json"),
  "qwsa221",
  callback
);

getCardsForListID(
  path.join(__dirname, "../data/cards.json"),
  "qwsa22",
  callback
);

getCardsForListID(path.join(__dirname, "../data/cards.json"), [], callback);

getCardsForListID(path.join(__dirname, "../data/cards.json"), "qwsa221");
