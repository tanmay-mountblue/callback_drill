const path = require("path");
const findListForGivenBoardID = require("../callback2");
const callback = (err, data) => {
  if (err) {
    console.error("Failure");
    console.error(err.message);
  } else {
    console.log("Success");
    console.log(data);
  }
};

findListForGivenBoardID(
  path.join(__dirname, "../data/lists.json"),
  "mcu453ed",
  callback
);

findListForGivenBoardID(
  path.join(__dirname, "../data/list.json"),
  "mcu453ed",
  callback
);

findListForGivenBoardID(
  path.join(__dirname, "../data/lists.json"),
  "mcu453e",
  callback
);

findListForGivenBoardID(
  path.join(__dirname, "../data/lists.json"),
  [],
  callback
);

findListForGivenBoardID(
  path.join(__dirname, "../data/lists.json"),
  "mcu453ed"
);