const path = require("path");
const problem5 = require("../callback5");

problem5(
  "Thanos",
  ["Mind", "Space"],
  path.join(__dirname, "../data/boards.json"),
  path.join(__dirname, "../data/lists.json"),
  path.join(__dirname, "../data/cards.json")
);
