const fs = require("fs");
const getBoardInformaton = require("./callback1");
const findListForGivenBoardID = require("./callback2");
const getCardsForListID = require("./callback3");

module.exports = (
  boardName,
  listName,
  boardsFilePath,
  listsFilePath,
  cardsFilePath
) => {
  setTimeout(() => {
    if (
      typeof boardsFilePath !== "string" ||
      typeof listsFilePath !== "string" ||
      typeof cardsFilePath !== "string" ||
      typeof boardName !== "string" ||
      typeof listName !== "string"
    ) {
      callback(new Error("Invalid Input!!!"));
    } else {
      fs.readFile(boardsFilePath, (err, data) => {
        if (err) {
          console.error(err.message);
        } else {
          const boardsData = JSON.parse(data);
          let boardData = boardsData.find((board) => board.name === boardName);

          if (!boardData) {
            console.error("Board Name Not Found!!!");
          } else {
            getBoardInformaton(boardsFilePath, boardData.id, (err, data) => {
              if (err) {
                console.error(err.message);
              } else {
                findListForGivenBoardID(listsFilePath, data.id, (err, data) => {
                  if (err) {
                    console.error(err.message);
                  } else {
                    const list = data.find((list) => list.name === listName);

                    if (!list) {
                      console.error("List Name Not Found!!!");
                    } else {
                      getCardsForListID(cardsFilePath, list.id, (err, data) => {
                        if (err) {
                          console.error(err.message);
                        } else {
                          console.log(data);
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        }
      });
    }
  }, 2000);
};
