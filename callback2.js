const fs = require("fs");

module.exports = (listsFilePath, boardID, callback) => {
  setTimeout(() => {
    if (typeof callback !== "function") {
      console.error("Callback is not a function!!!");
    } else if (
      typeof listsFilePath !== "string" ||
      typeof boardID !== "string"
    ){
      callback(new Error("Invalid Input!!!"));
    } else {
      fs.readFile(listsFilePath, (err, data) => {
        if (err) {
          callback(err);
        } else {
          try {
            const listsData = JSON.parse(data);

            const listData = listsData[boardID];

            if (!listData) {
              callback(new Error("List Data Not Found!!!"));
            } else {
              callback(null, listData);
            }
          } catch {
            callback(new Error("Data Not in JSON Format!!!"));
          }
        }
      });
    }
  }, 2000);
};
