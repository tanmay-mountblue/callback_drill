const fs = require("fs");

module.exports = (boardsFilePath, boardID, callback) => {
  setTimeout(() => {
    if (typeof callback !== "function") {
      console.error("Callback is not a function!!!");
    } else if (
      typeof boardsFilePath !== "string" ||
      typeof boardID !== "string"
    ) {
      callback(new Error("Invalid Input!!!"));
    } else {
      fs.readFile(boardsFilePath, (err, data) => {
        if (err) {
          callback(err);
        } else {
          try {
            const boardsData = JSON.parse(data);

            const boardInformation = boardsData.find(
              (boardData) => boardData.id === boardID
            );

            if (boardInformation) {
              callback(null, boardInformation);
            } else {
              callback(new Error("No Data Found!!!"));
            }
          } catch {
            callback(new Error("Data Not in JSON Format!!!"));
          }
        }
      });
    }
  }, 2000);
};
