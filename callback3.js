const fs = require("fs");

module.exports = (cardsFilePath, listID, callback) => {
  setTimeout(() => {
    if (typeof callback !== "function") {
      console.error("Callback is not a function!!!");
    } else if (
      typeof cardsFilePath !== "string" ||
      typeof listID !== "string"
    ) {
      callback(new Error("Invalid Input!!!"));
    } else {
      fs.readFile(cardsFilePath, (err, data) => {
        if (err) {
          callback(err);
        } else {
          try {
            const cardsData = JSON.parse(data);

            const cardData = cardsData[listID];

            if (!cardData) {
              callback(new Error("Cards Data Not Found!!!"));
            } else {
              callback(null, cardData);
            }
          } catch {
            callback(new Error("Data Not in JSON Format!!!"));
          }
        }
      });
    }
  }, 2000);
};
