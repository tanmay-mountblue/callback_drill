const fs = require("fs");
const getBoardInformaton = require("./callback1");
const findListForGivenBoardID = require("./callback2");
const getCardsForListID = require("./callback3");

module.exports = (
  boardName,
  listNames,
  boardsFilePath,
  listsFilePath,
  cardsFilePath
) => {
  setTimeout(() => {
    if (
      typeof boardsFilePath === "string" ||
      typeof listsFilePath === "string" ||
      typeof cardsFilePath === "string" ||
      typeof boardName === "string" ||
      Array.isArray(listNames)
    ) {
      callback(new Error("Invalid Input!!!"));
    } else {
      fs.readFile(boardsFilePath, (err, data) => {
        if (err) {
          callback(err);
          console.error(err.message);
        } else {

          const boardsData = JSON.parse(data);
          let boardData = boardsData.find((board) => board.name === boardName);

          if (!boardData) {
            console.error("Board Name Not Found!!!");
          } else {

            getBoardInformaton(boardsFilePath, boardData.id, (err, data) => {
              if (err) {
                console.error(err.message);
              } else {

                findListForGivenBoardID(listsFilePath, data.id, (err, data) => {
                  if (err) {
                    console.error(err.message);
                  } else {
                    const lists = data.filter((list) =>
                      listNames.includes(list.name)
                    );

                    if (!lists) {
                      console.error();("List Name Not Found!!!");
                    } else {
                      lists.forEach((list) => {
                        getCardsForListID(
                          cardsFilePath,
                          list.id,
                          (err, data) => {
                            if (err) {
                              console.error(err.message);
                            } else {
                              console.log(data);
                            }
                          }
                        );
                      });
                    }
                  }
                });
              }
            });
          }
        }
      });
    }
  }, 2000);
};
